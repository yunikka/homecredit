    @staticmethod
    def game_settings_pull():
        """ Получение текущих игровых настроек """

        cmd = 'icasino2/game_settings'
        url = urljoin(settings.IGROSOFT_URL, cmd)
        headers = CasinoApi._prepare_headers()

        logger.debug('GET to API: url=%s headers=%s' % (url, headers))

        r = requests.get(url, headers=headers)

        logger.debug("Response. %s %s" % (r.status_code, r.text))
        if r.status_code == 200:
            data = json.loads(r.text)

            return data

        else:
            raise ValidationError(_('API returned %s status') % r.status_code)

