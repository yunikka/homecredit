#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import os, sys

if len(sys.argv) == 1:
    print "usage ./script.py <connector_list> <interval> <delivery_proc> <min_amount>\nExample:\n./graphite.py '517,518,617,618' 10 30 20"
    sys.exit(3)

CONFIG = {
    'url' : 'http://graphiteapi.zagruzka.com',
    'connector_list' : str(sys.argv[1]),
    'interval' : '-%s' % str(sys.argv[2]),
    'delivery_proc' : int(sys.argv[3]),
    'min_amount' : int(sys.argv[4]),
}

# map nagios errors to exit codes
ERRORS = { 
    'OK' : '0',
    'WARNING' : '1',
    'CRITICAL' : '2',
    'UNKNOWN' : '3',
}

# main function
def main():

    # получаем данные по метрике total
    total = get_data(CONFIG['url'],CONFIG['connector_list'],'total',CONFIG['interval'])

    # Проверка на минимальный порог или 0
    if total < CONFIG['min_amount'] or total == 0:
        print("Too little messages (total %s)" % total)
        sys.exit(2)

    # получаем данные по метрике delivered
    delivered =  get_data(CONFIG['url'],CONFIG['connector_list'],'delivered',CONFIG['interval'])

    result_proc = round(100*float(delivered)/float(total), 2)
    print('%s%% delivered, total %s for last %s minutes' % (result_proc, total, CONFIG['interval'][1:]))

    if result_proc < CONFIG['delivery_proc']:
#         print("CRITICAL")
         sys.exit(2)
    else:
#         print("OK")
         sys.exit(0)

# функция получения данных из графаны
def get_data(url, connector_list, metric, interval):

    result = 0

    for connector_id in CONFIG['connector_list'].split(","):

        try:
            r = requests.get('%s/render?target=smsc.glassfish.connector_%s.%s&format=json&from=%smin' % (url, connector_id, metric, interval))
        except requests.exceptions.RequestException as e:
            print('Unable connect to %s' % url)
            sys.exit(3)

        parsed_string = json.loads(r.text[1:-1]) # обрезаем лишнии символы вначале и вконце строки

        sum = 0
        for col in parsed_string['datapoints']:
            if col[0] is not None:
                sum += int(col[0]) # захватываем только данные целевой метрики
            else:
                pass
        result += sum

    return result

# call main function
if __name__ == '__main__':
    main()

# eof
