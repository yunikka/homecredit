#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb as mdb
import sys

from single_process import single_process
from timeout import timeout

CONFIG = {
    # db config
    'host' : 
        [
            'mysql-smsinfo-rw.hints.svzn.net',
            'mysql-aggr-rw.hints.svzn.net',
            'mysql-ussd-rw.hints.svzn.net', 
        ],
    'db' : 'smpp_server',
    'mysql_user' : 'nagios',
    'mysql_passwd' : 'nagios',


    # префикс для графаны
    'prefix' : 'smsc.smpp_server',

    # smpp_sessions_monitoring fields
    'session_fields':
        [
            'mt_receive_speed',
            'mt_sent_to_aggr_speed',
            'mt_processing_speed',
            'mo_sending_speed',
            'statuses_sending_speed',
        ],

    # smpp_partners_monitoring fields
    'partner_fields':
        [
            'active_sessions',
            'mt_sent',
            'mo_sent',
            'statuses_sent',
            'mt_in_queue',
            'mo_in_queue',
            'statuses_in_queue',
            'statuses_awaiting',
            'statuses_pool_size',
        ],

    # sql queries    
    'sql_session' : 'select `system_id`, round(SUM(`mt_receive_speed`),2), round(SUM(`mt_sent_to_aggr_speed`),2), round(SUM(`mt_processing_speed`),2), round(SUM(`mo_sending_speed`),2), round(SUM(`statuses_sending_speed`),2) from `smpp_sessions_monitoring` GROUP BY `system_id`;',
    'sql_partner' : 'select `system_id`, SUM(`active_sessions`), SUM(`mt_sent`), SUM(`mo_sent`), SUM(`statuses_sent`), SUM(`mt_in_queue`), SUM(`mo_in_queue`), SUM(`statuses_in_queue`), SUM(`statuses_awaiting`), SUM(`statuses_pool_size`) from `smpp_partners_monitoring` GROUP BY `system_id`;'

}

@single_process
@timeout(300)
def main():

    for host in CONFIG['host']:

        data = Graph(host,CONFIG['mysql_user'],CONFIG['mysql_passwd'],CONFIG['db'])
        result_sessions = data.return_metric(CONFIG['sql_session'],CONFIG['session_fields'])
        result_partner = data.return_metric(CONFIG['sql_partner'],CONFIG['partner_fields'])

        prefix = '%s.%s' % (CONFIG['prefix'], host.split('.')[0])
        for result in result_sessions, result_partner:
            list_prefix(prefix,result)

class Graph(object):
    '''Собирает значения метрик по заданному полю в запросе'''
    def __init__(self, host, mysql_user, mysql_passwd, db):
        self.host = host
        self.mysql_user = mysql_user
        self.mysql_passwd = mysql_passwd
        self.db = db

    def return_metric(self, sql, fields):

        value = {}

        # open database connection
        connectMysql = mdb.connect(self.host,self.mysql_user,self.mysql_passwd,self.db)

        # prepare a cursor object using cursor() method
        cursor = connectMysql.cursor()

        try:
            cursor.execute(sql)
            result = cursor.fetchall()

            # заполняем словарь
            for col in result:
                temp = {col[0]: dict(zip(fields, col[1:]))}
                value.update(temp)
                    
        except mdb.OperationalError, e:
            print "Error: unable to fetch data"
            sys.exit(2)

        # disconnect from server
        connectMysql.close()

        return value

def list_prefix(prefix, dictionary):
    '''Распечатываем результат подставляя префикс'''
    for system_id in dictionary:
#        print(dictionary[system_id])
        for value_name in dictionary[system_id]:
#            print(dictionary[system_id][value_name])
            print('%s.%s.%s %s' % (prefix, system_id, value_name, dictionary[system_id][value_name]))

# call main function
if __name__ == '__main__':
    main()

# eof
