from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.views import logout
from django.views.generic import ListView

from django.contrib.auth.models import User
from .models import Driver, Period


def not_authenticated_or_staff(func):
    '''Декоратор возвращает на страницу login, если пользователь не авторизован или является staff'''
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('login'))
        elif request.user.is_staff:
            logout(request)
            return HttpResponseRedirect(reverse('login'))
        else:
            return func(request, *args, **kwargs)
    return wrapper

@not_authenticated_or_staff
def DriverInfo(request):
   
    template="drivers/info.html"
    user = User.objects.get(username=request.user.username)
    driver = Driver.objects.get(user_id=user.id)
    last_period = Period.objects.filter(driver_name=driver.id).order_by('-start_date')[0]
    
    return render(request, template, {"user":user, "driver":driver, "last_period":last_period})
    
class PeriodsView(ListView):
    model = Period
    template_name = 'drivers/periods.html'
    context_object_name = 'periods'

    def get_queryset(self):
        '''Выводит транзакции только по текущему пользователю'''
        
        user = User.objects.get(username=self.request.user.username)
        driver = Driver.objects.get(user_id=user.id)
        
        return Period.objects.filter(driver_name=driver.id) 
