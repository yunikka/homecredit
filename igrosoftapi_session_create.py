    @staticmethod
    def session_create(**kwargs):
        """ создать сессию в игрософте, вернуть sesionID, launch, token и пр """
        game = kwargs['game']
        denom = kwargs['denom']
        currency = kwargs['currency']
        makeTransaction = settings.IGROSOFT_MAKETRANSACTION

        data = urllib.parse.urlencode({'game': game, 'denom': denom, 'currency': currency, 'makeTransaction': makeTransaction})

        cmd = 'icasino2/session_create'
        url = urljoin(settings.IGROSOFT_URL, cmd)
        headers = API._prepare_headers()

        req = urllib.request.Request(url, data=data, headers=headers, method="POST")

        with urllib.request.urlopen(req) as r:
            if r.status == 200:
                return json.loads(r.read().decode('utf-8'))
            else:
                raise IgrosoftAPIException()
